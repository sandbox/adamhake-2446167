<?php
/**
 * @file node_reference.inc
 */

/**
 * Implements _webform_defaults_component()
 */
function _webform_defaults_node_reference() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'mandatory' => 0,
    'extra' => array(
      'field_prefix' => '',
      'field_suffix' => '',
      'node_types' => '',
    ),
  );
}

/**
 * Implements _webform_theme_component()
 */
function _webform_theme_node_reference() {
  return array(
    'webform_display_node_reference' => array(
      'render element' => 'element'
    )
  );
}

/**
 * Implements _webform_edit_component()
 */
function _webform_edit_node_reference($component) {
  $form = array();
  $form['extra']['node_reference_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node Reference Configuration'),
    '#collapsible' => TRUE
  );
  $form['extra']['node_reference_config']['entity_types'] = array(
    '#title' => t('Allowed Node Types'),
    '#description' => t('Select node types to query.  If none select, all node types will be queried.'),
    '#type' => "checkboxes",
    '#options' => _webform_node_reference_node_type_options(),
    '#default_value' => !empty($component['extra']['node_types']) ? $component['extra']['node_types'] : array(),
    '#parents' => array('extra', 'node_types'),
  );
  return $form;
}

/**
 * Implements _webform_render_component()
 */
function _webform_render_node_reference($component, $value = NULL, $filter = TRUE) {
  $element = array(
    '#type' => 'textfield',
    '#title' => $filter ? _webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before', 
    '#default_value' => $filter ? _webform_filter_values($component['value'], NULL, NULL, NULL, FALSE) : $component['value'], 
    '#required' => $component['mandatory'], 
    '#weight' => $component['weight'], 
    '#field_prefix' => empty($component['extra']['field_prefix']) ? NULL : ($filter ? _webform_filter_xss($component['extra']['field_prefix']) : $component['extra']['field_prefix']), 
    '#field_suffix' => empty($component['extra']['field_suffix']) ? NULL : ($filter ? _webform_filter_xss($component['extra']['field_suffix']) : $component['extra']['field_suffix']), 
    '#description' => $filter ? _webform_filter_descriptions($component['extra']['description']) : $component['extra']['description'],
    '#webform_component' => $component,
    '#autocomplete_path' => 'webform-node-reference/autocomplete/' . $component['cid'] . "/" . $component['nid'],
    '#allowed_entities' => _webform_node_reference_format_allowed_node_types($component)
  );
  return $element;
}

/**
 * Gets list of allowed node types and formats them into an array
 */
function _webform_node_reference_format_allowed_node_types($component) {
  $entity_types = array();
  foreach ($component['extra']['node_types'] as $name => $value) {
    if ($value) {
      $entity_types[] = $name;
    }
  }
  if (empty($entity_types)) {
    return FALSE;
  }
  return $entity_types;
}